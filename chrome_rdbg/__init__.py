try:
    import ujson as json
except ImportError:
    import json

import os

with open(os.path.join(os.path.dirname(__file__), 'protocol',
    'browser_protocol.json')) as f:
    browser = json.loads(f.read())

version = browser['version']
domains = [d['domain'] for d in browser['domains']]


def get_domain(name):
    global browser

    return browser['domains'][domains.index(name)]


class ProtocolMetaclass(type):
    def __new__(mcs, name, bases, namespace, **kw):
        global browser, version, domains

        if name not in domains:
            raise TypeError('Unknown protocol object')

        domain = get_domain(name)

        methods = dict(namespace)

        # print(domain.get('types'), domain.get('commands'),
        #         domain.get('events'))



        result = type.__new__(mcs, name, bases, methods)
        return result


class DOM(metaclass=ProtocolMetaclass):
    pass
