from setuptools import setup, find_packages

setup(
    name='chrome-rdbg',
    version='0.0a1',
    description='Chrome Remote Debug Protocol interface',
    packages=find_packages(),
    include_package_data=True,
    package_data={
        'chrome_rdbg': 'protocol/*.json',
        },
    install_requires=[],
    url='https://gitlab.com/sloat/chrome-rdbg',
    author='Matt Schmidt',
    author_email='matt@mattptr.net',
    license='MIT',
    classifiers=[
        'Development Status :: 2 - Pre-alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    )

